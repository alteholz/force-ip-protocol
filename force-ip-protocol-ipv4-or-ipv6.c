/*
   force-ip-protocol
   Copyright (C) 2023 Thorsten Alteholz

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   usage: LD_PRELOAD=/path/to/library.so program args ...
 */

#include <config.h>

#ifdef HAVE_SYS_SOCKET_H
 #include <sys/socket.h>
#endif

#ifdef HAVE_NETDB_H
 #include <netdb.h>
#endif

#ifdef HAVE_DLFCN_H
 #include <dlfcn.h>
#endif

int getaddrinfo(const char *hostname, const char *servname,
		const struct addrinfo *hints, struct addrinfo **res)
{
	int (*getaddrinfo_org)();
	struct addrinfo *hints2 = (struct addrinfo *) hints;

	getaddrinfo_org = dlsym(RTLD_NEXT, "getaddrinfo");
#if defined USE_IPV4
	hints2->ai_family = PF_INET;
#elif defined USE_IPV6
	hints2->ai_family = PF_INET6;
#else
#error You must define either USE_IPV4 or USE_IPV6.
#endif

	return (getaddrinfo_org(hostname, servname, hints2, res));
}
