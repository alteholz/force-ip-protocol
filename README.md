# force-ip-protocol

## Description

This library forces other software to use only IPv4 or IPv6 for network connections.

A library will be LD_PRELOADed which replaces function getaddrinfo()
by its own version. This version limits the available sockets to ones of
type PF_INET/PF_INTE6 and such limits all network traffic to a certain protocol

## Installation

Please see INSTALL.

## Usage

ipv4  wget http://www.t-online.de

## Authors and acknowledgment

Please see AUTHORS.

## License

This software is licensed under GPL-2.0-only.

